
let miMatriz = [
    ["A", "B", "C"],
    ["D", "E", "F"],
    ["G", "H", "I"]
];
function armarTablero() {
    let tablero = document.getElementById("tablero");
    let pos = 1;
    for (let i = 0; i < 9; i++) {
        tablero.innerHTML = tablero.innerHTML +
            "<input type='text' class='casilla'  id='casilla" + pos + "' onClick='realizarJugada()'></input>";
        if ((i + 1) % 3 === 0) {
            tablero.innerHTML = tablero.innerHTML + "<br>";
        }
        pos++;
    }
    tablero.innerHTML = tablero.innerHTML +
        "<input class='boton' type='button' value='X' id='boton1' onclick='alternarBotones()'>";
    tablero.innerHTML = tablero.innerHTML +
        "<input class='boton' type='button' value='O' id='boton2' onclick='alternarBotones()'>";
}
function alternarBotones(triqui) {
    let boton1 = document.getElementById('boton1');
    let boton2 = document.getElementById('boton2');
    let casillas = document.getElementsByClassName('casilla');
    if (triqui) {
        boton1.disabled = true;
        boton2.disabled = true;
        casillas.disabled = true;
    } else {
        if (boton1.disabled === true) {
            boton1.disabled = false;
            boton2.disabled = true;
        } else {
            boton1.disabled = true;
            boton2.disabled = false;
        }
    }
}
function realizarJugada() {
    let miCuadro = event.target;
    let miMensaje = document.getElementById("mensaje");
    if (!(miCuadro.value === "X" || miCuadro.value === "O")) {
        let boton1 = document.getElementById('boton1');
        let valor;
        if (boton1.disabled === true) {
            valor = 'O';
        } else {
            valor = 'X';
        }
        console.log(valor);
        miCuadro.value = valor;
        //Aquí llenar la matriz
        let pos = 1;
        //filas y columnas
        for (let y = 0; y < 3; y++) {
            for (let i = 0; i < 3; i++) {
                miMatriz[y][i] = document.getElementById(`casilla${pos}`).value;
                pos++;
            }
        }
        triqui = verificarTriqui();
        alternarBotones(triqui);
        if (triqui == true) {
            alert('¡GANA!');
            location.reload();
            return true;
            
        }
        miMensaje.innerText = "";
    } else {
        miMensaje.innerText = "Este cuadro ya está ocupado!!!!";     
    }
}
function verificarTriqui() {
    let triqui = false;
    //Verificar si hay triqui en las filas
    for (let i = 0; i < 3; i++) {
        if (miMatriz[i][0] === 'O' && miMatriz[i][1] === 'O' && miMatriz[i][2] === 'O') {
            triqui = true;
        }
        if (miMatriz[i][0] === 'X' && miMatriz[i][1] === 'X' && miMatriz[i][2] === 'X') {
            triqui = true;
        }
    }
    //Verificar si hay triqui en las columnas
    for (let j = 0; j < 3; j++) {
        if (miMatriz[0][j] === 'O' && miMatriz[1][j] === 'O' && miMatriz[2][j] === 'O') {
            triqui = true;
        }
        if (miMatriz[0][j] === 'X' && miMatriz[1][j] === 'X' && miMatriz[2][j] === 'X') {
            triqui = true;
        }
    }
    // Para verificar si hay triqui en las diagonales
    if (miMatriz[0][0] === 'O' && miMatriz[1][1] === 'O' && miMatriz[2][2] === 'O') {
        triqui = true;
    }
    if (miMatriz[0][0] === 'X' && miMatriz[1][1] === 'X' && miMatriz[2][2] === 'X') {
        triqui = true;
    }
    if (miMatriz[0][2] === 'O' && miMatriz[1][1] === 'O' && miMatriz[2][0] === 'O') {
        triqui = true;
    }
    if (miMatriz[0][2] === 'X' && miMatriz[1][1] === 'X' && miMatriz[2][0] === 'X') {
        triqui = true;
    }
    return triqui;
}